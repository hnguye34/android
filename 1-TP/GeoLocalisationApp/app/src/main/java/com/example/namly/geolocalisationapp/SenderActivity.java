package com.example.namly.geolocalisationapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity to send the position to the GeoLocalisation Server
 */
public class SenderActivity extends AppCompatActivity {
    private GeoServerParameter geoServerParameter;
    Button btSend;
    TextView tvResult;
    EditText edId;
    EditText edPasswd;
    EditText edLongi;
    EditText edRadius;
    EditText edLatitude;
    EditText edAdd;
    EditText edMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);
        btSend =  findViewById(R.id.btSend);
        tvResult =  findViewById(R.id.tvResult);
        edId =  findViewById(R.id.edId);
        edPasswd =  findViewById(R.id.edPasswd);
        edLatitude =  findViewById(R.id.edtAttitude);
        edLongi =  findViewById(R.id.edtLongi);
        edRadius=  findViewById(R.id.edtRaduis);
        edAdd =  findViewById(R.id.edtUrl);
        edMsg =  findViewById(R.id.edMessage);
        geoServerParameter = new GeoServerParameter(this);
        String prefUrl = geoServerParameter.getPrefURL();
        edAdd.setText(prefUrl);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendingMesssageTask.AsyncResponse response = new SendingMesssageTask.AsyncResponse() {
                    @Override
                    public void sendReponse(SendingMesssageTask.Result result) {
                        if ( result.getCode() == SendingMesssageTask.Result.Code.OK)
                            tvResult.setText(getResources().getString(R.string.SEND_OK));
                        else {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append(getResources().getString(R.string.SEND_KO))
                                    .append(":").append(result.getDetail());
                            tvResult.setText(stringBuilder.toString());
                        }
                    }

                };
                SendingMesssageTask sendingMessageTask = new SendingMesssageTask(response);


                geoServerParameter.setURL(edAdd.getText().toString());
                String url = geoServerParameter.getHttpUrl();
                sendingMessageTask.execute(url,
                        edPasswd.getText().toString(),
                        edId.getText().toString(),
                        edLatitude.getText().toString(),
                        edLongi.getText().toString(),
                        edRadius.getText().toString(),
                        edMsg.getText().toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        // Check the right modes
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.SettingItem:
                Intent intent = new Intent(this, GeoLocalisationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity( intent);
                return true;
            default:
                return false;
        }
    }
}
