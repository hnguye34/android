package com.example.namly.geolocalisationapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Class to store every parameters and setting relatives to the
 * GeoLocationsation Server
 * Also, it saves the last used Url address and the choice on Notificaiton filter
 */
public class GeoServerParameter {
    private static final String GEO_SVC_URL_REQUEST = "requests";
    private static final String GEO_SVC_URL = "url";
    private static final String WS_URL_DEFAULT = "10.0.2.2:1818";
    private static final String GEO_NOTIF_FILTERED = "Notif_Filter_Enable";
    public static final String GEOSRV_MTD_NAME = "/notifications/android";
    private final Context context;
    private SharedPreferences prefs ;
    GeoServerParameter( Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(GEO_SVC_URL_REQUEST, Context.MODE_PRIVATE);
        getPrefURL();
    }
    public String getServiceURL() {
        String prefUrl = getPrefURL();

        String url = "ws://" + prefUrl + GEOSRV_MTD_NAME;
        return url;
    }
     String getHttpUrl () {
        String prefUrl = getPrefURL();

        String url = "http://" + prefUrl + GEOSRV_MTD_NAME;
        return url;
    }
     boolean getNotifFilter () {
        boolean isFilter = prefs.getBoolean(GEO_NOTIF_FILTERED, false);
        return isFilter;

    }

    public boolean setNotifFilter ( boolean isFilter) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(GEO_NOTIF_FILTERED, isFilter).commit();
        return getNotifFilter();
    }


    public String getPrefURL() {
        String prefUrl = prefs.getString(GEO_SVC_URL, null);
        if ( prefUrl == null)
            prefUrl = setURL(WS_URL_DEFAULT);

        return prefUrl;
    }

    String setURL(String url) {
        String prefUrl = prefs.getString(GEO_SVC_URL, null);
        if ( prefUrl != null && prefUrl.equals(url))
            return prefUrl;

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(GEO_SVC_URL, url).commit();

        return url;
    }
}
