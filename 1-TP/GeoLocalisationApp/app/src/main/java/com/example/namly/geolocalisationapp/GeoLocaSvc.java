package com.example.namly.geolocalisationapp;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

/**
 * The service to connect to GeoLocation Server
 * The service is unique. It is auto-stopped when losing connection with the server
 */
public class GeoLocaSvc extends Service {


    public static final String URL_KEY = "url";
    public static final String GEO_SERVER_NOT_AVAILABLE = "NOT_AVAILABLE";
    public static final String GEO_LOCALISATION_NOT_AVAILABLE = "GEO_LOCALISATION_NOT_AVAILABLE";
    private static final int LOCATION_TIMEOUT = 10;
    private static final String TAG = "GeoLocaSvc";
    private static OkHttpClient client;
    private final Binder binder = new LocalBinder(); //https://developer.android.com/guide/components/bound-services
    WebSocket ws;
    private Handler handler;
    private Context context = this;
    private volatile boolean positionIsFiltered = false;
    NetworkUtil networkObserver = new NetworkUtil();
    BroadcastReceiver eventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(NetworkUtil.C_INTERNET_LOST)) {
                NotificationUtil.createToast(context, "Reseau non disponible");
                stopServiceNow();
            }
        }
    };


    public GeoLocaSvc() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        NotificationUtil.createChannel(this);
        handler = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                if (msg.what == MessageListener.MessageType.SVC_FAILURE.getValue()) {
                    stopServiceNow();
                    return true;
                }
                if (positionIsFiltered)
                    filterMessageByPosition(msg);
                else
                    publish(msg);

                return true;

            }
        });

        client = new OkHttpClient();
        NotificationUtil.toNotif(this, "Service démarré");

        IntentFilter filterNetwork = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(this.networkObserver, filterNetwork);
        IntentFilter internetAvailable = new IntentFilter(NetworkUtil.C_INTERNET_LOST);
        this.registerReceiver(eventReceiver, internetAvailable);
    }

    private void stopServiceNow() {
        stopSelf();
    }

    /**
     *  The service will be stopped.
     *  Notify this event then free the broadcast Receiver
     *  To avoid un exception error when freeing twice the broadcast Receiver
     *  surround it by try/catch
     */
    @Override
    public void onDestroy() {
        context.sendBroadcast(new Intent(GEO_SERVER_NOT_AVAILABLE));
        if (ws != null) {
            ws.close(1000, null);
            NotificationUtil.createToast(context, "Arrêt connexion serveur");
        }
        NotificationUtil.createToast(context, "Arrêt service géo notification ");
        try  {
            this.unregisterReceiver(this.networkObserver);
            this.unregisterReceiver(eventReceiver);
        }
        catch (Exception e) {

        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        return this.binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

            String url = intent.getStringExtra(URL_KEY);
            startWatching(url);

            return super.onStartCommand(intent, flags, startId) ;
    }



    private void startWatching(String url) {
        // the URL must start with ws:// or wss:// (and not http:// or https://)

        try {
            Request request = new Request.Builder().url(url).build();
            MessageListener listener = new MessageListener();
            listener.subscribe(this.handler);

            ws = client.newWebSocket(request, listener);

        } catch (Exception e) {
            context.sendBroadcast(new Intent(GEO_SERVER_NOT_AVAILABLE));

        }

    }

    private void publish(Message message) {
        String text = MessageListener.toText(message);
        NotificationUtil.createToast(context, text);
        NotificationUtil.toNotif(context, text);
    }

    private void treatMessage(Message message, Location myLoc) {
        if (myLoc == null) // no position so not to publish notification anyway
        {
            publish(message);
            return;
        }
        Location msgLocation = new Location(myLoc);
        double radius = NotificationUtil.getRadius(MessageListener.toText(message), msgLocation);

        float distance = myLoc.distanceTo(msgLocation);
        if (distance <= radius)
            publish(message);
        else {
            Log.v(TAG, msgLocation.toString());
            Log.v(TAG, myLoc.toString());
        }

    }

    void filterMessageByPosition(Message msg) {
        LocationManager lm;
        try {
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        } catch (Exception e) {
            context.sendBroadcast(new Intent(GEO_LOCALISATION_NOT_AVAILABLE));
            return;
        }
        @SuppressLint("MissingPermission") Location loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER); // no effort since we use an already known location
        if (loc != null) {
            // location already known
            treatMessage(msg, loc);
        } else {
            final boolean[] treatedMessage = {false};
            // we must ask Android to watch for the location
            LocationListener listener = new LocationListener() {


                @Override
                public void onLocationChanged(Location location) {
                    if (!treatedMessage[0]) { // if the message has not already been treated
                        treatMessage(msg, location);
                        treatedMessage[0] = true;
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                @Override
                public void onProviderEnabled(String provider) {
                }

                @Override
                public void onProviderDisabled(String provider) {
                }
            };
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                context.sendBroadcast(new Intent(GEO_LOCALISATION_NOT_AVAILABLE));
                return;
            }
            lm.requestSingleUpdate(LocationManager.GPS_PROVIDER, listener, null /* using main thread */);
            Handler hdl = new Handler();

            hdl.postDelayed(() -> {
                if (!treatedMessage[0]) {
                    treatMessage(msg, null);
                    treatedMessage[0] = true;
                }
            }, LOCATION_TIMEOUT);
        }

    }

    public synchronized void setIsNotficationFiltered(boolean checked) {
        positionIsFiltered = checked;
    }

    class LocalBinder extends Binder {

        GeoLocaSvc getService() {
            return (GeoLocaSvc.this);
        }
    }
}
