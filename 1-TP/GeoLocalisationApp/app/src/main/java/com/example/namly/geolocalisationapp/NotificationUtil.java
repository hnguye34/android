package com.example.namly.geolocalisationapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;

import android.location.Location;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;
import java.util.HashMap;


/**
 *  utility class
 *  To push a notification
 *  To display a toast
 *  To parse the notifcation and return the radius value
 */
 class NotificationUtil {
    private static final String CHANNEL_ID = "com.example.namly.geolocalisationapp";
    private static final String CHANNEL_NAME = "MyChannelName";

    /*
        Create the channel for foreground service
        Must be called once in OnCreate of the service
     */
     static void createChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /*
        send a notification
     */
     static Notification toNotif(android.content.Context context, String text) {
        NotificationCompat.Builder b = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setOngoing(true)
                .setContentText(text)
                .setContentTitle("notifications")
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setChannelId(CHANNEL_ID)
                .setSmallIcon(android.R.drawable.ic_notification_overlay);

        Notification notif = b.build();
        int id = (int) System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notif);
        return (notif);
    }

    /**
     * display a toast
     * @param context
     * @param text
     */
    static void createToast(Context context, String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.RIGHT, 0, 0);
        toast.show();
    }

    static  double getRadius(String text, Location msgLocation) {


        HashMap<String, String> fieldPairs  = new HashMap<>();

        try {

            for (String fields : text.replace("{", "").replace("}","").split(",")) {
                String[] fieldNames = fields.split(":");
                fieldPairs.put(fieldNames[0].replace("\"",""), fieldNames[1]);
            }

            msgLocation.setLatitude(Double.parseDouble(fieldPairs.get("latitude")));
            msgLocation.setLongitude(Double.parseDouble(fieldPairs.get("longitude")));

            double radius = Double.parseDouble(fieldPairs.get("radius"));
            return radius;
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.v("NotificationUtil", "Exception", e);
            return 0;
        }
    }
}
