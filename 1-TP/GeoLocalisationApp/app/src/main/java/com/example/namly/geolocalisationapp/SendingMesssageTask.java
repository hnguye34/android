package com.example.namly.geolocalisationapp;

import android.os.AsyncTask;
import android.util.Log;



import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

/**
 * Task to send a message to GeoLocalisation server
 * It publishs the Result object using Observer pattern principle
 */
public class SendingMesssageTask extends AsyncTask<String, Void, SendingMesssageTask.Result> {

    /**
     * Result of the message sending
     * In error case, the details contains the error cause
     */
    public static class Result
    {
        public enum Code {            OK, KO }
        private final Result.Code code;
        private final String detail;
        Result (Result.Code code, String detail) {
            this.code = code;
            this.detail = detail;
        }
        Code getCode() { return code;}
        String getDetail() { return detail;}
    }

    public interface AsyncResponse {
        void sendReponse(Result result);
    }

    private final  AsyncResponse handler ;
    SendingMesssageTask (AsyncResponse handler) {
        this.handler = handler;
    }
    private static final String TAG = "SendingMesssageTask";
    private class Encoder {
        private StringBuilder postData = new StringBuilder();

        private Encoder encode(String param, String value) throws UnsupportedEncodingException {

            if (postData.length() > 0)
                postData.append("&");

            postData.append(URLEncoder.encode(param, "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(value, "UTF-8"));
            return this;
        }


        private String getString() {
            return postData.toString();
        }
    }



    protected Result doInBackground(String... strings) {
        HttpURLConnection conn = null;
        Result result = new Result(Result.Code.OK, "");
        try {
        Encoder encoder = new Encoder();
        String passwd = strings[1];
        encoder.encode("id", strings[2])
                .encode("latitude", strings[3])
                .encode("longitude", strings[4])
                .encode("radius", strings[5]);
        String message = strings[6];
            String strUrl =  strings[0] + "?" + encoder.getString();
            URL url = new URL(strUrl);


        byte[] postDataBytes = message.getBytes("UTF-8");


        conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("X-Password", passwd);
        conn.setRequestProperty("Connection", "close"); // workaround
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("charset", "utf-8");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setFixedLengthStreamingMode(postDataBytes.length);
       // conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoInput(true);
        conn.setDoOutput(true);
        try ( OutputStream output = conn.getOutputStream() ){
            output.write(postDataBytes);
            output.flush();
        }

        if ( conn.getResponseCode() == HttpURLConnection.HTTP_OK )
            result = new Result(Result.Code.OK, "");
        else
            result = new Result(Result.Code.KO, conn.getResponseMessage());
        }
        catch (UnsupportedEncodingException e) {
            result = new Result(Result.Code.KO, e.getMessage());
            e.printStackTrace();
            Log.v(TAG, "exception", e);
        } catch (ProtocolException e) {
            result = new Result(Result.Code.KO, e.getMessage());
            e.printStackTrace();
            Log.v(TAG, "exception", e);
        } catch (MalformedURLException e) {
            result = new Result(Result.Code.KO, e.getMessage());
            e.printStackTrace();
            e.printStackTrace();
            Log.v(TAG, "exception", e);
        } catch (IOException e) {
            result = new Result(Result.Code.KO, e.getMessage());
            e.printStackTrace();
            Log.v(TAG, "exception", e);
        }
        catch (Exception e) {
            result = new Result(Result.Code.KO, e.getMessage());
            e.printStackTrace();
            Log.v(TAG, "exception", e);
        }
        finally {
            if (conn != null)
                conn.disconnect();

        }
        return result;
   }

    @Override
    protected void onPostExecute(Result s) {
        if ( this.handler != null)
            handler.sendReponse(s);
    }
}

