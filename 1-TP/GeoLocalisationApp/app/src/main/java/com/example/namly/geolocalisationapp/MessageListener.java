package com.example.namly.geolocalisationapp;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 *  Listenner to receive the message sent from GeoLocalisation Server
 *  It notify the creator class all events happenning , using the handler
 *  with Observer pattern principle.
 */
public class MessageListener extends WebSocketListener {
    public enum MessageType { SVC_FAILURE (1) ,
        NORMAL_MESSAGE (2);
        private int val;
         MessageType(int value) {
            this.val = value;
        }

        public int getValue() {
            return val;
        }

    }

    private static final String TAG = "MessageListener";
    private static final String C_KEY = "Key";
    private static final String PUSH_SINCE = "pushSince:0";
    private Handler handler;

    @Override
    public void onOpen(WebSocket webSocket, Response response) {

        System.out.println("onOpen: ");
        webSocket.send(PUSH_SINCE);

    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        System.out.println("onMessage: ");
        if (handler == null)
            return;

        Message msg = new Message();
        Bundle bundle = new Bundle();
        bundle.putString(C_KEY, text);
        msg.what = MessageType.NORMAL_MESSAGE.getValue();
        msg.setData(bundle);
        handler.sendMessage(msg);
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        System.out.println("MESSAGE: " + bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(1000, null);
        Message msg = new Message();
        msg.what =  MessageType.SVC_FAILURE.getValue();

        Bundle bundle = new Bundle();
        bundle.putString(C_KEY, "Connection closed");
        msg.setData(bundle);

        handler.sendMessage(msg);
        System.out.print("Connection closed");
        Log.v(TAG, "Connection closed");
    }

    static   String toText(Message message) {
        return message.getData().getString(C_KEY);
    }
    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {

        Message msg = new Message();
        msg.what =  MessageType.SVC_FAILURE.getValue();

        Bundle bundle = new Bundle();
        bundle.putString(C_KEY, t.getMessage());
        msg.setData(bundle);

        handler.sendMessage(msg);
        System.out.print(t.getStackTrace());
        Log.v(TAG, "exception", t);
    }

     void subscribe(Handler handler) {
        this.handler = handler;
    }


}
