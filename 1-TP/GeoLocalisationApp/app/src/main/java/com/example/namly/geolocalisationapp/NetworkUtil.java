package com.example.namly.geolocalisationapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Broadcast Receiver to watch over the connectivity network
 * and publish the event when network connection is lost
 *
 * Do not forget to add the permission
 *  <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
 *
 */
public   class NetworkUtil extends BroadcastReceiver {

    public static final String C_INTERNET_AVAILABLE = "INTERNET_AVAILABLE";
    public static final String C_INTERNET_LOST = "INTERNET_LOST";
    @Override
    public void onReceive(Context context, Intent intent) {
        if ( isOnline(context) )
            context.sendBroadcast(new Intent(C_INTERNET_AVAILABLE));
        else
            context.sendBroadcast(new Intent(C_INTERNET_LOST));
    }

    private boolean isOnline(Context context)
    {
        boolean isOnline = false;
        try
        {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            isOnline = (netInfo != null && netInfo.isConnected());

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return isOnline;
    }


}
