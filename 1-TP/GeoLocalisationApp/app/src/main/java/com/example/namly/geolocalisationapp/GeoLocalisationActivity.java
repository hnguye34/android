package com.example.namly.geolocalisationapp;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Activity to start/stop the notification and the filter
 */
public class GeoLocalisationActivity extends AppCompatActivity {
    private static final int GEOLOCATION_REQUEST_CODE = 1;
    CheckBox ckStart;
    CheckBox ckGeo;
    EditText edtUrl;
    Boolean networkUtilIsRegistered = false;


    BroadcastReceiver notifReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(GeoLocaSvc.GEO_SERVER_NOT_AVAILABLE)) {
                NotificationUtil.createToast(context, " Server géocalisation non joignable");
                ckStart.setChecked(false);
            } else if (intent.getAction().equals(GeoLocaSvc.GEO_LOCALISATION_NOT_AVAILABLE)) {
                NotificationUtil.createToast(context, "la localisation impossible");
                ckGeo.setChecked(false);
            }
        }
    };


    private GeoServerParameter geoServerParameter;
    private GeoLocaSvc geoLocaSvc = null;
   private ServiceConnection onService = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            geoLocaSvc = ((GeoLocaSvc.LocalBinder) rawBinder).getService();

            if (ckStart != null) {
                ckStart.setChecked(geoLocaSvc != null);
                ckStart.setEnabled(true);
            }
            if (ckGeo != null && geoLocaSvc != null) {
                geoLocaSvc.setIsNotficationFiltered(ckGeo.isChecked());
                ckGeo.setEnabled(true);
            }
        }

        public void onServiceDisconnected(ComponentName className) {

            geoLocaSvc = null;

            if (ckStart != null) {
                ckStart.setChecked(false);
                ckStart.setEnabled(true);
            }
        }
    };

    /** when activity is openned,
     * Verify whether the notification service is running and so check the checkbox
     * Display the last Url address used
     * Restore the last choice about notification filter
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_localisation);
        geoServerParameter = new GeoServerParameter(this);
        ckStart = (CheckBox) findViewById(R.id.ckStart);
        edtUrl = (EditText) findViewById(R.id.edtUrl);
        ckGeo = (CheckBox) findViewById(R.id.ckGeo);
        // We restore the choice on
        boolean isNotifFilter = this.geoServerParameter.getNotifFilter();
        ckGeo.setChecked(isNotifFilter);
        String prefUrl = geoServerParameter.getPrefURL();
        edtUrl.setText(prefUrl);
        ckStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickStartNotification(v);
            }

        });

        ckGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickckGeo(v);
            }

        });
    }

    private void onClickckGeo(View v) {

        if (this.ckGeo.isChecked()) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GEOLOCATION_REQUEST_CODE);
            else
                filterNotification(true);
        } else {
            filterNotification(false);
        }
    }

    private void filterNotification(boolean isFiltered) {

        if (this.geoLocaSvc != null) {
            this.geoLocaSvc.setIsNotficationFiltered(isFiltered);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case GEOLOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                boolean isGeo = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
                ckGeo.setChecked(isGeo);
                filterNotification(isGeo);
                return;
            }
        }
    }


    @Override
    protected void onResume() {
        if (!networkUtilIsRegistered) {
            IntentFilter intentFiler = new IntentFilter();
            intentFiler.addAction(GeoLocaSvc.GEO_SERVER_NOT_AVAILABLE);
            registerReceiver(notifReceiver, intentFiler);
            networkUtilIsRegistered = true;
        }

        super.onResume();
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(notifReceiver);
            networkUtilIsRegistered = false;
        } catch (Exception e) {

        }
        this.geoServerParameter.setNotifFilter(this.ckGeo.isChecked());
        super.onPause();


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isSvcRunning()) {
            ckStart.setEnabled(false);
            bindService(new Intent(this, GeoLocaSvc.class), onService, Context.BIND_AUTO_CREATE);
        } else
            ckStart.setChecked(false);
    }

    @Override
    protected void onStop() {
        if (geoLocaSvc != null) {
            this.unbindService(this.onService);
        }
        super.onStop();
    }

    void startService(String myUrl) {
        Intent intent = new Intent(this, GeoLocaSvc.class)
                .putExtra(GeoLocaSvc.URL_KEY, myUrl);
        this.ckStart.setChecked(false); //will be checked only when svc is connected
        this.startService(intent);
        bindService(new Intent(this, GeoLocaSvc.class), onService, Context.BIND_AUTO_CREATE);
    }

    /**
     * Enable/Disable notification when clicking on checkbox
     * @param view
     */
    private void onClickStartNotification(View view) {
        if (ckStart.isChecked()) {

            if (!edtUrl.getText().toString().isEmpty())
                geoServerParameter.setURL(edtUrl.getText().toString());
            else
                edtUrl.setText(geoServerParameter.getPrefURL());

            String serviceUrl = geoServerParameter.getServiceURL();

            if (serviceUrl.isEmpty()) {
                this.ckStart.setChecked(false);
            }

            this.startService(serviceUrl);
        } else
            this.stopService();
    }

    void stopService() {
        this.stopService(new Intent(this, GeoLocaSvc.class));
    }

    private boolean isSvcRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (GeoLocaSvc.class.getName().equals(service.service.getClassName())) {

                return true;
            }
        }


        return false;

    }
}
